#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <inttypes.h>
#include <stdlib.h>
#include "matrix.h"
#include "multiply.h"

void multiply_random_matrices_of_dimension(unsigned int N);
double get_wall_clock_time();
void print_metrics(int N, double wall_clock_time, double cpu_time);
void print(int n, int m[n][n]);

#define DEFAULT_N 2 // dimension of square matrix
#define NULL_TIMEZONE NULL

/**
Usage: ./mm 1 10
1  - Start from 1x1 Matrices
10 - Repeat 10 times, each time doubling dimension N.
*/
int main(int argc, char const *argv[]) {

  printf("Matrices Multiplier v0.1\n");
  printf("argc: %d\n", argc);
  for (int i = 0; i < argc; i++) {
    printf("argv[%d]: %s\n", i, argv[i]);
  }

  int N = DEFAULT_N;
  unsigned repeats = 1;
  switch (argc) {
    case 2:
      N = strtoimax(argv[1],NULL,10);
      break;
    case 3:
      N = strtoimax(argv[1],NULL,10);
      repeats = strtoumax(argv[2],NULL,10);
      break;
    default:
      break;
  }

  printf("N, Wall Clock Time, CPU Time\n");

  for (size_t i = 0; i < repeats; i++, N*=2) {
    // capture time before
    double start = get_wall_clock_time();
    clock_t cpu_start = clock();

    multiply_random_matrices_of_dimension(N);

    // capture time after
    double end = get_wall_clock_time();
    clock_t cpu_time = clock() - cpu_start;
    print_metrics(N, end - start, (float)cpu_time/CLOCKS_PER_SEC);
  }

  return 0;
}

void multiply_random_matrices_of_dimension(unsigned int N) {
  int **a = matrix_create(N, N);
  int **b = matrix_create(N, N);
  int **c = matrix_create(N, N);

  unsigned seed = time(NULL);
  matrix_seed_random(seed);
  matrix_populate_random(N, a);
  matrix_populate_random(N, b);

  multiply(N, a, b, c);

  matrix_destroy(N, a);
  matrix_destroy(N, b);
  matrix_destroy(N, c);
  // print(N, a);
  // print(N, b);
  // print(N, c);
}

double get_wall_clock_time() {
  struct timespec t;
  int errno = clock_gettime(CLOCK_REALTIME, &t);
  if (errno != 0) {
    exit(errno);
  }
  return (double)t.tv_sec + (double)t.tv_nsec * 1e-9;
}

void print_metrics(int N, double wall_clock_time, double cpu_time) {
  printf("%d, %.9f, %.9f\n", N, wall_clock_time, cpu_time);
}

/**
 @param: m the matrix to print
*/
void print(int n, int m[n][n]) {
  for (int x = 0; x < n; x++) {
    for (int y = 0; y < n; y++) {
      printf("[%d][%d]=%d\n", x, y, m[x][y]);
    }
  }
  printf("\n");
}
