#include <stdio.h>
#include <time.h>
#include "test_utils.h"
#include "matrix.h"

int test_populate_random(void);
int test_create_2_by_2_matrix(void);
int test_create_1024_by_1024_matrix(void);

int main(void) {

  printf("Running tests...\n");

  int result = 0;
  result += test_populate_random();
  result += test_create_2_by_2_matrix();
  result += test_create_1024_by_1024_matrix();

  if (result) {
    printf("There were test failures.\n");
  } else {
    printf("PASSED! :)\n");
  }
  return result;
}

int test_populate_random(void) {
  int N = 2;
  int **act = matrix_create(2, 2);
  int **exp = matrix_create(2, 2);

  exp[0][0] = 383;
  exp[0][1] = 886;
  exp[1][0] = 777;
  exp[1][1] = 915;

  unsigned seed = 1;
  matrix_seed_random(seed);
  matrix_populate_random(N, act);

  return assert_equal(N, exp, act);
}

int test_create_2_by_2_matrix(void) {

  int **m = matrix_create(2, 2);
  // can access the element
  m[0][0] = 0x55;
  m[0][1] = 0xAA;
  m[1][0] = 0xAA;
  m[1][1] = 0x55;

  int row1[] = {0x55, 0xAA};
  int row2[] = {0xAA, 0x55};
  int *exp[] = { &row1[0], &row2[0] };

  return assert_equal(2, exp, m);
}

int test_create_1024_by_1024_matrix(void) {

  int **m = matrix_create(1024, 1024);

  m[1020][1020] = 0x55;

  int result = 0;
  result += (m[1020][1020] == 0x55) ? 0: 1;

  matrix_destroy(1024, m);
  return result;
}
