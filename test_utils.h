#ifndef TEST_UTILS_H
#define TEST_UTILS_H

int assert_equal(int n, int **expected, int **actual);

#endif
