## Compiler ##
# -O2 Optimize even more. GCC performs nearly all supported optimizations that do not involve a space-speed tradeoff. See https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html
CFLAGS = -Wall -Wextra -O2
CC     = gcc $(CFLAGS)

## Program ##
objects = main.o matrix.o multiply.o
exes = matrices-multiply test-matrix test-multiply

mm : $(objects)
	$(CC) -o mm $(objects)

test-matrix : test_matrix.o matrix.o test_utils.o
	$(CC) -o test-matrix test_matrix.o matrix.o test_utils.o

test-multiply : test_multiply.o test_utils.o multiply.o
	$(CC) -o test-multiply test_multiply.o test_utils.o multiply.o

clean:
	-rm $(objects) $(exes)
