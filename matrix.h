#ifndef MATRIX_H

int** matrix_create(unsigned row, unsigned col);
void  matrix_destroy(unsigned n, int **matrix);
void  matrix_seed_random(unsigned seed);
void  matrix_populate_random(int n, int **matrix);
#define MATRIX_H
#endif
