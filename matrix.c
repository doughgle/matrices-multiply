#include <stdlib.h>
#include "matrix.h"

#define MAX_RANDOM_VALUE 1000

/**
 return a pointer to a matrix m where elements can be accessed by m[row][col]
*/
int** matrix_create(unsigned rows, unsigned cols) {
  int **m = malloc(rows * sizeof *m);

  // init addresses pointing to start of each row
  // store address of the array of ints for each row
  for (unsigned row = 0; row < rows; row++) {
    m[row] = malloc(cols * sizeof *m[row]);
  }
  return m;
}

void matrix_destroy(unsigned rows, int **matrix) {
  for (unsigned row = 0; row < rows; row++) {
    free(matrix[row]);
  }
  free(matrix);
  matrix = NULL;
}

/**
 Example seed argument: unsigned seed = time(NULL);
 Call this only once per execution of the program.
*/
void matrix_seed_random(unsigned seed) {
  srand(seed);
}

/**
 Populates matrix a with random integers where rand() is seeded by seed.
*/
void matrix_populate_random(int n, int **a) {
  for (int x = 0; x < n; x++) {
    for (int y = 0; y < n; y++) {
      a[x][y] = rand() % MAX_RANDOM_VALUE;
    }
  }
}
