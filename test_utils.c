#include <stdio.h>
#include "test_utils.h"

int assert_equal(int n, int **expected, int **actual) {
  int result = 0;
  for (int x = 0; x < n; x++) {
    for (int y = 0; y < n; y++) {
      if (expected[x][y] != actual[x][y]) {
        printf("FAIL @(%d,%d): expected %d, got %d\n", x, y, expected[x][y], actual[x][y]);
        result = 1;
      }
    }
  }

  return result;
}
