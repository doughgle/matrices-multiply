#include "multiply.h"

/**
 !Beware - Doesn't handle integer overflow!
*/
int multiply(int n, int **a, int **b, int **c) {
  for (int x = 0; x < n; x++) {
    for (int y = 0; y < n; y++) {
      if(a[x][y] == 0 || b[x][y] == 0) {
        c[x][y] = 0;
      } else {
        c[x][y] = a[x][y] * b[x][y];
      }
    }
  }
  return 0;
}
