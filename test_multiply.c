#include <stdio.h>
#include "test_utils.h"
#include "multiply.h"

int test_multiply_2_by_2_matrices(void);
int test_multiply_3_by_3_matrices();
int test_multiply_by_zero(void);
int test_multiply_integer_overflow();

int main(int argc, char const *argv[]) {

  printf("Running tests...\n");

  int result = 0;
  result += test_multiply_2_by_2_matrices();
  result += test_multiply_3_by_3_matrices();
  result += test_multiply_by_zero();
  // result += test_multiply_integer_overflow();

  if (result) {
    printf("There were test failures.\n");
  } else {
    printf("PASSED! :)\n");
  }
  return result;
}

int test_multiply_2_by_2_matrices(void) {
  #define N 2
  int a[N][N] = {
    {2, 4}, {6, 8}
  };

  int b[N][N] = {
    {1, 3}, {5, 9}
  };

  int expected[N][N] = {
    {2, 12}, {30, 72}
  };

  int c[N][N] = {};
  int ret = multiply(N, a, b, c);

  return assert_equal(N, expected, c);
}

int test_multiply_3_by_3_matrices() {
  int a[3][3] = {
    {1, 2, 3},
    {4, 5, 6},
    {7, 8, 9}
  };

  int b[3][3] = {
    {2, 4, 6},
    {8, 10, 12},
    {14, 16, 18}
  };

  int expected[3][3] = {
    {2, 8, 18},
    {32, 50, 72},
    {98, 128, 162}
  };

  int c[3][3] = {};
  int ret = multiply(3, a, b, c);

  return assert_equal(3, expected, c);
}

int test_multiply_by_zero(void) {
  #define N 2

  int a[N][N] = {
    {2, 4}, {6, 8}
  };

  int b[N][N] = {
    {0, 1}, {1, 0}
  };

  int expected[N][N] = {
    {0, 4}, {6, 0}
  };

  int c[N][N] = {};
  int ret = multiply(N, a, b, c);

  return assert_equal(N, expected, c);
}

int test_multiply_integer_overflow() {

  int a[1][1] = {
    {7294}
  };

  int b[1][1] = {
    {1}
  };

  int expected[1][1] = {
    {7294}
  };

  int c[1][1] = {};
  int ret = multiply(N, a, b, c);

  return assert_equal(N, expected, c);
}
